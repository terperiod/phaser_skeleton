﻿/// <reference path="thirdparty/phaser.d.ts" />
/// <reference path="common/references.ts" />
/// <reference path="masterData/references.ts" />

/// <reference path="sequence/ready/references.ts" />
/// <reference path="sequence/title/references.ts" />
/// <reference path="sequence/main/references.ts" />


class Sequencer {
    constructor() {
        // develop
        DebugMode.instance.setActive();
        Profiler.instance.activate();
        //

        ResolutionManager.instance.initialize(
            Constants.GameWidth,
            Constants.GameHeight
        );

        let game = new Phaser.Game(
            ResolutionManager.instance.width,
            ResolutionManager.instance.height,
            Phaser.AUTO,
            "content",
            {
                preload: this.preload,
                create: this.create,
                update: this.update,
                render: this.render
            });
        game.preserveDrawingBuffer = true;
        PhaserEngine.instance.initialize(game);
    }


    preload() {
        let game = PhaserEngine.instance.game;

        ResourceManager.instance.initialize();
        ResourceManager.instance.preload(progress => {
            Bridge.instance.updateLoadProgress(progress);
        });

        game.stage.setBackgroundColor(Constants.bgColor);
    }

    create() {
        Bridge.instance.startGame(()=>{
            InputManager.instance.initialize();
            Sequencer.startSequence();
        });
    }

    update() {
        if (Pause.instance.paused)
            return;

        if (Sequencer.currentSequence != null)
            Sequencer.currentSequence.tick();
    }

    render() {
    }


    private static currentSequence: SequenceGame = null;

    private static startSequence() {
        Bridge.instance.onPaused = ()=>{
            Pause.instance.pause();
        };
        Bridge.instance.onResumed = ()=>{
            Pause.instance.resume();
        };
        Pause.instance.onPaused = () => {
            PauseView.attach();
        };

        let sequences = new KVSMap<SequenceGame>();
        sequences.register(
            GameReady.Tag,
            new GameReady());
        sequences.register(
            GameTitle.Tag,
            new GameTitle());
        sequences.register(
            GameMain.Tag,
            new GameMain());
                                    
        for (let sequence of sequences.getValues()) {
            sequence.onFinished = tag => {
                InputManager.instance.clear();
                let next: SequenceGame = sequences.get(tag);
                Sequencer.currentSequence = next;
                if (Sequencer.currentSequence != null) {
                    Sequencer.currentSequence.initialize();
                } else {
                    Bridge.instance.quit();
                }
            };
        }

        Sequencer.currentSequence = sequences.get(GameReady.Tag);
        Sequencer.currentSequence.initialize();
     }
}

window.onload = () => {
    Bridge.instance.initialize(()=>{
        let sequencer = new Sequencer();
    });
};