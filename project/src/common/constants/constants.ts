﻿class Constants {
    public static bgColor: string = "#000000";

    public static get GameWidth(): number{
        return 480;
    }
    public static get GameHeight(): number{
        return 800;
    }

    // after ResolutionManager.initialize() ony
    public static get ScreenH() {
        return window.innerHeight / ResolutionManager.instance.scale;
    }
}