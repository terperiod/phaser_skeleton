﻿class SoundTags {
    public static get MainBgm() { return "main_bgm"; }

    private soundFileMap: KVSMap<string> = new KVSMap<string>();

    constructor() {
    }

    public getSoundPath(tag: string) {
        return this.soundFileMap.get(tag);
    }

    public get preloadSoundTags() {
        let list: Array<string> = new Array<string>();
        return list;
    }
}