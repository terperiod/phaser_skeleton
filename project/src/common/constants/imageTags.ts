﻿class ImageTags {
    public static get White() { return "white"; }
    public static get Tinkle() { return "cross"; }
    public static get Sphere() { return "sphere"; }
    public static get Gradation() { return "gradation"; }
    public static get DefaultExpandableFrames() {
        let map = new KVSMap<string>();
        map.register(Image9SliceTags.LeftBottom, "frame1_1");
        map.register(Image9SliceTags.Bottom, "frame1_2");
        map.register(Image9SliceTags.RightBottom, "frame1_3");
        map.register(Image9SliceTags.Left, "frame1_4");
        map.register(Image9SliceTags.Center, "frame1_5");
        map.register(Image9SliceTags.Right, "frame1_6");
        map.register(Image9SliceTags.LeftTop, "frame1_7");
        map.register(Image9SliceTags.Top, "frame1_8");
        map.register(Image9SliceTags.RightTop, "frame1_9");
        return map;
    }

    private imageMap: KVSMap<string> = new KVSMap<string>();
    private atlasMap: KVSMap<string> = new KVSMap<string>();
    private tagAtlasMap: KVSMap<string> = new KVSMap<string>();
    
    constructor() {
        this.imageMap.register(ImageTags.White, "res/images/white.png");
        this.imageMap.register(ImageTags.Tinkle, "res/images/cross.png");
        this.imageMap.register(ImageTags.Sphere, "res/images/sphere.png");
        this.imageMap.register(ImageTags.Gradation, "res/images/gradation.png");

        for (let frame of ImageTags.DefaultExpandableFrames.getValues())
            this.imageMap.register(frame, "res/images/" + frame + ".png");
    }

    public getImagePath(tag: string) {
        return this.imageMap.get(tag);
    }

    public getAtlasPath(tag: string) {
        return this.atlasMap.get(tag);
    }

    public get preloadImageTags() {
        let list: Array<string> = new Array<string>();
        list.push(ImageTags.White);
        list.push(ImageTags.Tinkle);
        list.push(ImageTags.Sphere);
        list.push(ImageTags.Gradation);

        for (let frame of ImageTags.DefaultExpandableFrames.getValues())
            list.push(frame);

        return list; 
    }
    
    public get preloadAtlasTags() {
        let list: Array<string> = new Array<string>();
        return list;
    }

    public getTextureAtlasName(tag: string): string {
        if (this.tagAtlasMap.get(tag) == null) {
            return "";
        }

        return this.tagAtlasMap.get(tag);
    }

    public isTextureAtlasName(tag: string): boolean {
        return this.getTextureAtlasName(tag) != "";
    }
}