﻿class FadeInEffectView extends OneShotEffectView {
    public static attach() {
        let view = new FadeInEffectView(new Atagoal.Position(
            Constants.GameWidth / 2,
            Constants.GameHeight / 2
        ));

        ViewManager.instance.register(view);
        return view;
    }

    public detach() {
        super.detach();
        ViewManager.instance.unregister(this);
    }

    constructor(position: Atagoal.Position) {
        super(position);
        this.setImage(ImageTags.White);
        this.setScaleX(2000);
        this.setScaleY(2000);
        this.image.tint = 0x000000;
    }

    public tick() {
        super.tick();

        let ratio = (this.duration - this.elaspedCount) / this.duration;
        this.image.alpha = ratio;
    }
}