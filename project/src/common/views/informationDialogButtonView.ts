﻿class InformationDialogButtonView extends View {
    public static attach(
        position:Atagoal.Position = new Atagoal.Position()) {
        let view = new InformationDialogButtonView(position);
        return view;
    }

    public setup(
        width:number,
        height:number,
        border:number,
        message:string,
        fontsize:number,
        callback:()=>void
    ){
        let images = new KVSMap<string>();
        for (let key of Image9SliceTags.ImageKeys)
            images.register(key, ImageTags.ButtonFrames.get(key));
        let frame = ExpandableImageView.attach().setImages(
            new Image9SliceTags().setImages(images).setSize(
                width,
                height,
                border
            )
        );
        frame.onFrameClicked = ()=>{
            callback();
        };
        frame.setParent(this.node);

        let text = this.addText(
            message,
            new Atagoal.Position(),
            fontsize,
            InformationDialogView.FontColor
        );
        text.anchor.x = text.anchor.y = 0.5;

        return this;
    }

    public get root():Phaser.Sprite {
        return this.node;
    }
}