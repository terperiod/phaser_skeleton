﻿class TextNumbersView extends View {
    public static attach(
        position: Atagoal.Position,
        fontSize: number = 16,
        color: string = "#000"
    ) {
        let view: TextNumbersView = new TextNumbersView(
            position,
            fontSize,
            color);

        ViewManager.instance.register(view);
        return view;
    }

    private text: Phaser.Text;

    constructor(
        position: Atagoal.Position,
        fontSize: number = 16,
        color: string = "#000"
    ) {
        super(position);

        let textFontSize = Math.floor(fontSize * ResolutionManager.instance.scale);

        this.text = PhaserEngine.game.add.text(
            0,
            0,
            "",
            {
                font: "bold " + textFontSize + "px Arial",
                fill: color,
                boundsAlignH: "center",
                boundsAlignV: "middle"
            });

        this.node.addChild(this.text);
    }

    public setup(
        basePosition: Atagoal.Position,
        fontSize: number = 16
    ) {
        this.setPosition(basePosition);
        this.text.fontSize = Math.floor(fontSize * ResolutionManager.instance.scale);
    }

    public updateNumbers(
        value: number) {
        this.text.text = value.toString();
    }

    private clearNumbers() {
        this.text.text = "0";
    }
}