﻿class PauseView extends ButtonView {
    public static attach() {
        let view = new PauseView(new Atagoal.Position(
            Constants.GameWidth / 2,
            Constants.GameHeight / 2
        ));

        return view;
    }

    public detach() {
        super.detach();
    }

    constructor(position: Atagoal.Position) {
        super(position);
        this.setButtonImage(ImageTags.White);
        this.setScaleX(2000);
        this.setScaleY(2000);
        this.button.tint = 0x000000;
        this.button.alpha = 0.4;

        let fontSize = 32 * ResolutionManager.instance.scale;
        let offset = -100;
        let text = PhaserEngine.game.add.text(
            (position.x + offset) * ResolutionManager.instance.scale,
            position.y * ResolutionManager.instance.scale,
            "Tap to resume",
            {
                font: "bold " + fontSize + "px Arial",
                fill: "#ccc",
                boundsAlignH: "left",
                boundsAlignV: "middle"
            });

        this.onClicked = ()=>{
            Pause.instance.resume();
            text.destroy();
            this.detach();
        };
    }
}