﻿class OneShotEffectView extends View {
    protected duration: number = 0;
    protected elaspedCount: number = 0;
    protected _onFinished: Phaser.Signal = new Phaser.Signal();
    public set onFinished(callback: () => void) {
        this._onFinished.add(callback);
    }
    public setDuration(value: number) {
        this.duration = value;
        return this;
    }

    public tick() {
        if (this.isFinished())
            return;

        this.elaspedCount++;

        if (this.isFinished()) {
            this.detach();
            this._onFinished.dispatch();
        }
    }

    public isFinished() {
        return this.elaspedCount >= this.duration;
    }
}