﻿class InformationDialogView extends View {
    public static attach(
        position:Atagoal.Position = new Atagoal.Position(
            Constants.GameWidth / 2,
            Constants.GameHeight / 2
        )) {
        let view = new InformationDialogView(position);
        return view;
    }

    public static FontColor:string = "#006";


    private vies:Array<View> = new Array<View>();
    public detatch() {
        super.detach();
    }

    public setFrame(
        width:number,
        height:number,
        border:number,
        frameTags:KVSMap<string> = ImageTags.InventoryFrames
    ) {
        let images = new KVSMap<string>();
        for (let key of Image9SliceTags.ImageKeys)
            images.register(key, frameTags.get(key));
        let frame = ExpandableImageView.attach().setImages(
            new Image9SliceTags().setImages(images).setSize(
                width,
                height,
                border
            )
        );
        this.addChild(frame);

        return this;
    }

    public addMessage(
        message:string,
        fontSize:number,
        offset:Atagoal.Position = new Atagoal.Position()
    ) {
        let text = this.addText(
            message,
            offset,
            fontSize,
            InformationDialogView.FontColor
        );
        text.anchor.x = text.anchor.y = 0.5;
        return this;
    }

    public addButton(
        width:number,
        height:number,
        border:number,
        message:string,
        callback:()=>void,
        fontSize:number = 18,
        offset:Atagoal.Position = new Atagoal.Position()
    ){
        let button = InformationDialogButtonView.attach(offset).setup(
            width,
            height,
            border,
            message,
            fontSize,
            ()=>{
                callback();
            }
        );
        this.addChild(button);
        return this;
    }
}