﻿class GameReady extends SequenceGame {
    public static get Tag() {
        return "GameReady";
    }

    // override
    public get tag() {
        return GameReady.Tag;
    }

    initialize() {
        super.initialize();

        new SerialProcess().add(finish=>{
            this.loadMasterData(()=>{
                finish();
            });
        }).add(finish=>{
            this.end(GameTitle.Tag);
            finish();
        }).flush();

        this.loadImages();
    }

    private loadMasterData(callback: () => void) {
        // perload
        let loader = new Phaser.Loader(PhaserEngine.game);

        loader.json("game_master", "res/data/game_master.json");

        new SerialProcess().add(finish=>{
            loader.onLoadComplete.addOnce(()=>{
                finish();
            });
            loader.start();
        }).add(finish=>{
            GameMasterData.instance.load(()=>{
                finish();
            });
        }).add(finish=>{
            callback();
            finish();
        }).flush();
    }

    private loadImages() {
        DelayedLoadManager.instance.registerAtlases(
            new Array<DelayedLoadAtlas>(
            )
        );
    }
}

