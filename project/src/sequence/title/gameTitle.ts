﻿class GameTitle extends SequenceGame {
    public static get Tag() {
        return "GameTitle";
    }

    // override
    public get tag() {
        return GameTitle.Tag;
    }

    initialize() {
        super.initialize();

        StartButtonView.attach(new Atagoal.Position(
            Constants.GameWidth / 2,
            Constants.GameHeight / 2 + 100
        )).onClicked = ()=>{
            this.end(GameMain.Tag);
        };
    }

}