﻿class StartButtonView extends ButtonView {
    public static attach(position:Atagoal.Position) {
        let view = new StartButtonView(position);
        return view;
    }

    constructor(position: Atagoal.Position) {
        super(position);

        let width = 300;
        let height = 80;

        this.setButtonImage(ImageTags.White);
        this.button.scale.x = width / 10;
        this.button.scale.y = height / 10;
        this.button.alpha = 0;

        ExpandableImageView.attach().setImages(
            new Image9SliceTags().setSize(
                width,
                height,
                8
            )
        ).setParent(this.node);

        let text = this.addText(
            "Start",
            new Atagoal.Position(
                0,
                0
            ),
            48,
            "#333");
        text.anchor.x = 
        text.anchor.y = 0.5;
    }
}