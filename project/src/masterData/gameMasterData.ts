﻿class GameMasterData {
    private static internalInstance: GameMasterData = null;
    public static get instance() {

        if (GameMasterData.internalInstance == null)
            GameMasterData.internalInstance = new GameMasterData();

        return GameMasterData.internalInstance;
    }
    constructor() {
        // assert a constructor as private
        if (GameMasterData.internalInstance != null)
            console.error("The constructor of GameMasterData was called by outside. Do not new() a Singleton Class directly.");
    }

    private _version:string = "version not set";

    public get version() {
        return this._version;
    }

    public load(callback: () => void) {
        let json:Object = PhaserEngine.game.cache.getJSON("game_master");

        if (json.hasOwnProperty("version"))
            this._version = json["version"];

        callback();
    }
} 