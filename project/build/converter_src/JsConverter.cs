using System;

namespace FBIGJSUtil
{
    public class Converter
    {
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                System.Console.WriteLine("invalid argument");
                return;
            }

            new Converter().Convert(args[0]);
        }

        public void Convert(string filePath)
        {
            var rawText = System.IO.File.ReadAllText(filePath);
            var convertedText = rawText.Replace("/* del--", "").Replace("--del */", "");
            System.IO.File.WriteAllText(filePath, convertedText);
        }
    }
}